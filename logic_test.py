def new_direction():
    global direction
    global row_test
    global column_test

    if direction == "RIGHT":
        direction = "DOWN"
    elif direction == "DOWN":
        direction = "LEFT"
    elif direction =="LEFT":
        direction = "UP"
    elif direction=="UP":
        direction = "RIGHT"
    else:
        direction = "ERROR"


def is_next_cell_tested():
    global matrix
    global row_test
    global column_test

    try:
        if(matrix[row_test][column_test]==0):
            return False
        else:
            return True
    except IndexError:
        return True

def move_index_back():
    global cells_tested
    global row_test
    global column_test

    cells_tested = cells_tested - 1

    if(direction=="RIGHT"):
        column_test = column_test - 1
    if(direction=="DOWN"):
        row_test = row_test - 1
    if(direction=="LEFT"):
        column_test = column_test + 1
    if(direction=="UP"):
        row_test = row_test + 1

def move_index_forward():
    global direction
    global row_test
    global column_test

    if(direction=="RIGHT"):
        column_test = column_test + 1
    if(direction=="DOWN"):
        row_test = row_test + 1
    if(direction=="LEFT"):
        column_test = column_test - 1
    if(direction=="UP"):
        row_test = row_test - 1


def test_cell():  
    global direction
    global row_test
    global column_test
    global cells_tested
    global matrix

    move_index_forward()
    if is_next_cell_tested():
        move_index_back()
    try:
        if(matrix[row_test][column_test]==0):
            matrix[row_test][column_test]=direction
        else:
            if cells_tested < cells_count:
                new_direction()
                cells_tested = cells_tested + 1
                test_cell()
    except IndexError:
        if cells_tested < cells_count:
            new_direction()
            cells_tested = cells_tested + 1
            test_cell()




cells_tested = 0
def run_test(row,column):

    global cells_tested
    global direction
    global column_test
    global row_test
    global matrix
    global cells_count

    matrix = [[0] * column for i in range(row)]
    cells_count = 0

    for y in range(row):
        to_print = ""
        for x in range(column):
            to_print = to_print + "[" + str(y) + "," + str(x) + "] "
            matrix[y][x] = 0
            cells_count = cells_count + 1

    direction = "RIGHT"

    cells_tested = 0

    row_test = int(0)
    column_test = int(0)

    while cells_tested < cells_count:
        if cells_tested == 0:
            matrix[row_test][column_test]=direction
            cells_tested = cells_tested + 1
        else:
            cells_tested = cells_tested + 1
            test_cell()

    return direction
        


#####################################
### [START PROGRAM]
#####################################   


# Receive the number of test cases
try:
    print("¿how many test cases? (T)")
    num_test_case = int(input())
    inputs = [[0] * 2 for i in range(num_test_case)]
    counter = 0
except:
    print("Must enter numeric values, try again, please")
    exit()

# Ask for values for each test
while counter < num_test_case:
    try:
        print("")
        print("===========TEST " + str(counter) +"=============")
        print("¿how many for N? (N = Rows)")
        row_n = int(input())
        inputs[counter][0] = row_n
        print("¿how many for M? (M = Columns)")
        column_m = int(input())
        inputs[counter][1] = column_m
        print("===========================")
        print("")
        counter = counter + 1
    except:
        print("Must enter numeric values, try again, please")

# Run Test

for y in range(len(inputs)):
    row = inputs[y][0]
    column = inputs[y][1]
    print(">>>>> TEST: "+str(y)+" <<<<<")
    print( "ROWS: " + str(row) + " COLUMNS: " + str(column) )
    print("DIRECTION: "+str(run_test(row,column)))
    print("")






